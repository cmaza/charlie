<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'php-mailer/src/Exception.php';
require 'php-mailer/src/PHPMailer.php';

$mail = new PHPMailer();

// Form Fields
$first_name = isset($_POST["first_name"]) ? $_POST["first_name"] : null;
$company_name = isset($_POST["company_name"]) ? $_POST["company_name"] : null;
$phone_number = isset($_POST["phone_number"]) ? $_POST["phone_number"] : null;
$company_email = isset($_POST["company_email"]) ? $_POST["company_email"] : null;
$referral_company_name = isset($_POST["referral_company_name"]) ? $_POST["referral_company_name"] : null;
$referral_contact_name = isset($_POST["referral_contact_name"]) ? $_POST["referral_contact_name"] : null;
$referral_phone_number = isset($_POST["referral_phone_number"]) ? $_POST["referral_phone_number"] : null;
$referral_email = isset($_POST["referral_email"]) ? $_POST["referral_email"] : null;

// Enter your email address. If you need multiple email recipes simply add a comma: email@domain.com, email2@domain.com
$toEmails = "hyun.kim@vensure.com,Julie.dower@vensure.com";
$fromName = "Vensure Contact Form";
$fromEmail = "vensure@vensure.com";
$subject = "REFERRAL FORM COMPLETION"; // email subject
$messageContent = "<p>Referring Information:</p>"; // content message

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($company_email != '') {

        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        $mail->From = $fromEmail;
        $mail->FromName = $fromName;

        if (strpos($toEmails, ',') !== false) {
            $email_addresses = explode(',', $toEmails);
            foreach ($email_addresses as $email_address) {
                $mail->addAddress(trim($email_address));
            }
        } else {
            $mail->addAddress($toEmails);
        }

        $mail->addReplyTo($company_email, $full_name);
        $mail->Subject = $subject;

        $first_name = isset($first_name) ? "Name: $first_name<br>" : '';
        $company_name = isset($company_name) ? "Company Name: $company_name<br>" : '';
        $phone_number = isset($phone_number) ? "Phone Number: $phone_number<br>" : '';
        $company_email = isset($company_email) ? "Company Email: $company_email<br>" : '';
        $attached_message = "<p>Referral Company Information</p>";
        $referral_company_name = isset($referral_company_name) ? "Company Name: $referral_company_name<br>" : '';
        $referral_contact_name = isset($referral_contact_name) ? "Contact Name: $referral_contact_name<br>" : '';
        $referral_phone_number = isset($referral_phone_number) ? "Phone Number: $referral_phone_number<br>" : '';
        $referral_email = isset($referral_email) ? "Email: $referral_email<br>" : '';

        $mail->Body = $messageContent . $first_name .$company_name . $phone_number . $company_email . $attached_message . $referral_company_name . $referral_contact_name . $referral_phone_number . $referral_email;

        if (!$mail->send()) {
            $response = array('response' => 'error', 'message' => $mail->ErrorInfo);
        } else {
            $response = array('response' => 'success');
        }

        echo json_encode($response);
    } else {
        $response = array('response' => 'error');
        echo json_encode($response);
    }

}
