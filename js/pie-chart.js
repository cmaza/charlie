$( document ).ready(function() {
    var active_wrapper = false;
    var active_pie = false;

    $('.card-square .btn-less').each(function(){
        $(this).css('display','none');
    });

if($('#defaultOpen').length){
    document.getElementById("defaultOpen").click();
}


$(window).scroll(function(event) {
    var scrollTop = $(window).scrollTop();
    var sa= $('.sa').offset();
    var sa_height = $('.sa').height();

    var si= $('.si').offset();
    var si_height = $('.si').height();
    var margin = 600;
    var margini = 350;


    if($('.chart').length){
        if(scrollTop >= (si.top - margini) && scrollTop <= (si.top + si_height)){
            runAnimation('pie');
        }else{
            stopAnimation('pie');
        }
    }
    if($('.chart-container').length){
        
        if(active_pie == false){
            if(scrollTop >= 500 && scrollTop <= 1110 ){
                runAnimation('bars');
                active_pie = true;
            }else{
                stopAnimation('bars');
                active_pie = false;
            }
        } else{
            if(scrollTop >= 500 && scrollTop <= 1110 ){
            }else{
                stopAnimation('bars');
                active_pie = false;
            }
        }
        
    }
    
    
    if($('.wrapper').length){
        if(active_wrapper == false){
            if(scrollTop >= (sa.top - margin) && scrollTop <= (sa.top + sa_height)){
                runAnimation('number');
                active_wrapper = true;
                setTimeout(function(){
                    stopAnimation('number');
                },1000);
                
            }else{
                active_wrapper = false;
            }
        }else{
            if(scrollTop >= (sa.top - margin) && scrollTop <= (sa.top + sa_height)){              
                active_wrapper = true;
            }else{
                active_wrapper = false;
            }
        }
    }

    function stopAnimation(elm){
        switch(elm){
            case 'pie':
                $('.chart').addClass( "paused" );
                $('.chart').removeClass( "run" );
                break;
            case 'bars':
                $('.bar').removeClass( " animation" );
                break;
            case 'number':
                $('.box').removeClass( " animation" );
                $('.box').addClass( " noanimation" );
                break;
        }
    }

    function runAnimation(elm){
        switch(elm){
            case 'pie':
                $('.chart').addClass( "run" );
                $('.chart').removeClass( "paused" );
                break;
            case 'bars':
                $('.bar').addClass( " animation" );
                break;
            case 'number':
                $('.box').removeClass( " noanimation" );
                $('.box').addClass( " animation" );
                break;
        }
    }


  });
});

function openTabs(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
  }

  function seeMore(number){
    document.getElementById('more-'+number).className += ' active';
    document.getElementById('btn-more-'+number).style.display = "none";
    document.getElementById('btn-less-'+number).style.display = "block";
    
}

function less(number){
    document.getElementById('more-'+number).className = 'more font-size-15';
    document.getElementById('btn-more-'+number).style.display = "block";
    document.getElementById('btn-less-'+number).style.display = "none";
}
  