<?php
$headerImage = 'clientcenter/OR/oregon-header.png';
$stateName = 'Oregon';
$stateMapImage = 'clientcenter/OR/client-center-state-Oregon.png';
?>
<section id="page-title" class="clientcenter-internals" data-bg-parallax="<?php echo basePathUrl().$headerImage;?>">
    <div class="bg-overlay"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section id="client-center" class="internals">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 m-b-20">
                <img src="<?php echo basePathUrl().$stateMapImage;?>" width="100%">
            </div>
            <div class="col-lg-7">
                <h4><?php echo $stateName;?></h4>
                <div class="section-spacer-5"></div>
                <p>Home to the some of the largest brands in the world, Oregon has historically been a great state for business. With VensureHR’s PEO services, our Oregon clients focus all of their time on growing their business, while we manage the payroll, HR, benefits, risk mangement, and workers’ compensation services.</p>
                <div class="section-spacer-5"></div>
                <div class="container-fluid table-responsive-sm">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr class="row">
                            <th class="col-sm-8 text-center">File</th>
                            <th class="col-sm-2 text-center">English</th>
                            <th class="col-sm-2 text-center">Espa&ntilde;ol</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Enrollment Packet</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Enrollment-Packet-EN-Minimal.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/Enrollment-Packet-SP-Minimal.pdf"><i class="far fa-file-pdf"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">OR W-4</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/form-or-W-4_101-402_2020.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/form-or-W-4_101-402_2020.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Form I-9</td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/USCIS-Form-I-9-2020.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">
                                <a target="_blank" href="<?php echo basePathUrl();?>clientcenter/home/USCIS-Form-I-9-2020.pdf"><i class="far fa-file-pdf" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr class="row">
                            <td class="col-sm-8 text-center">Other State & Local Forms</td>
                            <td class="col-sm-2 text-center">
                                <a href="https://www.symmetry.com/resources/blank-forms-page" target="_blank"><i class="fas fa-external-link-alt"></i></a>
                            </td>
                            <td class="col-sm-2 text-center">N/A</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="section-spacer-5"></div>
                <p><strong>Nota importante:</strong>
                    <em>En estos momentos no existe un formulario I-9 en espa&ntilde;ol, por lo tanto, usted deber&aacute; descargar la versi&oacute;n en ingl&eacute;s y usar las
                        instrucciones en espa&ntilde;ol.</em></p>
            </div>
        </div>
    </div>
</section>

