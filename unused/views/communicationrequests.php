<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/VensureHR-communication-request-hero.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Communication Requests</h1>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-30"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <iframe width="100%" height="2200px" src="https://www.wrike.com/form/eyJhY2NvdW50SWQiOjMyNzA1NzgsInRhc2tGb3JtSWQiOjMwMDcwMX0JNDczNjQyNjk3MTYwMgkzYTAwYWQ0ZjQ5ZDRmMThmMjRlMzNjZDFiMDNmY2M4ODRmNDBmMjM5NGY5OTE5ZmZhNjY1YjdlYzQ4NzBjZGQ0" frameborder="0"></iframe>
            </div>
        </div>
        <div class="section-spacer-30"></div>
    </div>
</section>

