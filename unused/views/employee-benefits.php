<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Benefits.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Employee Benefits</h1>
            <span>Customized & Designed with Your Needs in Mind</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Benefits-Img1.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Employee Benefits Services</h4>
                    <div class="inside-spacer"></div>
                    <p>A competitive <strong>benefits plan</strong> is crucial to not only attracting the right talent but retaining quality employees. VensureHR’s benefits
                        program is tailored to your specific business at an industry-best cost. Regardless of company size, location, or length of operation
                        <strong>VensureHR</strong> can help create the perfect benefits package offerings.</p>
                    <p>From handling the ins and outs of the benefits program by collecting and remitting the insurance premium, to coordinating enrollments and
                        assisting co-employees with questions and concerns. Our clients have the freedom to use their existing benefits, VensureHR Sponsored Benefits, or both.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Major Medical</h4>
                    <div class="inside-spacer"></div>
                    <p>Access to group health insurance finally makes sense. Employers know that offering an attractive <strong>benefits package</strong> directly affects the
                        recruiter’s talent pool and the ability to retain current employees.</p>
                    <p>VensureHR’s health plans offer employers multiple PPO and HSA options to meet the employer’s health insurance requirements.
                        We deliver flexibility when comparing to commercial insurance, as well as additional cost-saving benefits.</p>
                    <p>Making benefits decisions that affect the entire employee base for a whole year is overwhelming. VensureHR provides the expert guidance
                        to navigate the complex choices associated with providing employees with affordable health coverage.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-Benefts-Major-Medical.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-Benefits-Dental.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Dental</h4>
                    <div class="inside-spacer"></div>
                    <p>Proactive dental care is a vital component of general health and well-being, and another key element of any good <strong>employee benefits package</strong>.
                        Regular dental visits and check-ups can identify and address health issues before they become severe and costly. VensureHR clients are able
                        to take advantage of our long-rooted relationships with nationwide carriers to receive industry-best coverage and plans regardless of company size or length of business</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Vision</h4>
                    <div class="inside-spacer"></div>
                    <p>According to the <a href="https://www.cdc.gov/mmwr/volumes/66/wr/mm6632a2.htm" target="_blank" class="internal">Centers for Disease Control and Prevention</a>,
                        nearly 45 million Americans wear contact lenses to help correct their vision*. Undiagnosed eye problems or poor vision can severely affect quality of
                        life and workplace productivity.</p>
                    <p>VensureHR’s vision insurance helps employees budget for ongoing vision care expenses, including routine eye exams, prescription glasses,
                        and contact lenses. With established relationships nationwide, employers now have true flexibility when it comes to selecting a perfect vision plan.
                        <a href="https://www.cdc.gov/mmwr/volumes/66/wr/mm6632a2.htm" target="_blank" class="internal"> *The CDC’s Morbidity and Mortality Weekly Report (MMWR)</a></p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-Benefts-Vision.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-Benefts-Retirement.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Retirement</h4>
                    <div class="inside-spacer"></div>
                    <p>VensureHR sponsors a multiple employer 401(k) plan through a strategic partnership with <a href="https://www.slavic401k.com/" target="_blank" class="internal">Slavic 401k</a>. The savings plans allow employees
                        to save for retirement while reducing their current taxable income.</p>
                    <p>Employers have the flexibility to select eligibility criteria, matching contribution formulas, profit sharing formulas, including integration and new comparability,
                        and safe harbor plans with vesting options.</p>
                    <p>90% of the plan is already designed</p>
                    <p>Cost per person is dramatically reduced</p>
                    <p>Plans are specifically designed to meet business requirements at a fraction of the cost</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Supplemental Products</h4>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Life</h3>
                        <p>Employee’s long-term financial planning strategy is primarily comprised of their life insurance policy. Designed to protect an employee’s family’s
                            financial security and to support dependents after death, life insurance provides extra peace-of-mind.</p>
                        <p>VensureHR’s group life insurance provides employers with essential protection for your employees at a competitive group rate.
                            Life can happen at any time—VensureHR can help reduce the financial burden during this difficult time.</p>
                    </div>
                    <div class="col-lg-12">
                        <h3>Short-Term Disability</h3>
                        <p>Even the smallest injuries can interfere with the ability to work. For many people, unplanned time away from work can make it difficult to manage
                            monthly bills and expenses. Short-term disability insurance provides supplementary income to reduce the financial stress of a covered illness or inju</p>
                    </div>
                    <div class="col-lg-12">
                        <h3>GAP</h3>
                        <p>A gap plan is a supplemental insurance plan to group medical, which is considered coinsurance. Adding gap insurance to certain major medical plans allows
                            employers to offer their employees the coverage they want at a cost they can afford.</p>
                    </div>
                    <div class="col-lg-12">
                        <h3>Accident</h3>
                        <p>The most recent report from the National Safety Council reveals that more than 44.5 million injuries happen each year at a cost of over $967.9 billion.
                            While accidents are never planned, sometimes there are financial gaps, even after health insurance. This is where Accidental policies come into play.
                            Lost wages from missed work, unmet or high health insurance deductibles, or even unpaid bills can be covered in an accidental injury situation.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Long-Term Disability</h3>
                        <p>Without warning, accidents, illness, and debilitating conditions resulting in added stress and financial strain. When an employee cannot work for an
                            extended period, long-term disability insurance helps reduce the financial worries by covering a portion of the employee’s missed income.
                            Long-term disability begins when the short-term disability policy reaches its end.</p>
                        <p>VensureHR’s team of experts guides businesses through the process of selecting an appropriate plan that fits the organization’s needs and budget.</p>
                    </div>
                    <div class="col-lg-12">
                        <h3>Critical Illness</h3>
                        <p>A critical illness insurance policy lessens the financial strain caused by a major illness, heart attack, or stroke. After a critical illness diagnosis,
                            the insured party receives a lump-sum percentage of the elected benefit amount to help pay for expenses, including deductibles, co-pays,
                            childcare, credit card bills, or travel for medical treatment.</p>
                    </div>
                    <div class="col-lg-12">
                        <h3>Hospital Indemnity</h3>
                        <p>Hospital Indemnity Insurance is not a replacement for traditional health insurance; rather it offers employees another source of funds when they encounter serious medical issues.
                            Benefits are paid to the insured to be used as they see fit.</p>
                    </div>
                    <div class="col-lg-12">
                        <h3>Telehealth</h3>
                        <p>Telehealth makes it easy to visit a doctor in minutes on the phone, mobile app, or web. Employees have access to board-certified doctors,
                            counselors, psychiatrists, or dermatologist without ever having the leave the comforts of their home. Convenient, private, and secure Telehealth this
                            one way that Vensure combines technology with a traditional PEO service.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">See How VensureHR Can Help Improve Your Business</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-40"></div>
</section>