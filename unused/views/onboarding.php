<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-New-Hire-Onboarding.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Onboarding</h1>
            <span>Ensure Success from Day One</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Onoarding-Program.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>A Better Onboarding Program</h4>
                <div class="inside-spacer"></div>
                <p>What if you could increase employee engagement simply by implementing a better new hire onboarding program? 60% of people
                    believe a good new hire onboarding process is the key to effectively integrating employees into the existing company culture.</p>
                <p class="p-t-20"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey">
    <div class="section-spacer-20"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="row">
                    <div class="col-lg-12">
                        <h4>Set the Stage</h4>
                        <p class="m-t-20">Create an effective onboarding program to give employees a solid foundation on their first day.</p>
                    </div>
                    <div class="col-lg-12">
                        <h4>Go Paperless</h4>
                        <p class="m-t-20">Provide new hires with a digital welcome kit to help acclimate them to their new team and company culture.</p>
                    </div>
                    <div class="col-lg-12">
                        <h4>Process Improvement</h4>
                        <p class="m-t-20">Regularly refine your existing onboarding process in addition to employee training, learning, and development.</p>
                    </div>
                    <div class="col-lg-12">
                        <h4>Role-Based Goals</h4>
                        <p class="m-t-20">Set collaborative goals that address both the objectives of the position and the employee’s professional goals.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Onoarding-Set-the-Stage.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR Solutions That Deliver the Freedom to Succeed</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
