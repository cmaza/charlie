<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-PEO-Services.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">PEO Services</h1>
            <span>Full-Service HR Solutions</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/videos/vensure-PEO-video-thumbnail.png" alt=""></a>
                        </div>
                        <div class="portfolio-description">
                            <a data-lightbox="iframe" href="https://vimeo.com/321836904?autoplay=1">
                                <i class="fas fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                    <h4>Freedom to Succeed</h4>
                    <div class="inside-spacer"></div>
                    <p><strong>VensureHR</strong> is one of the nation's fastest growing <strong>professional employer organizations (PEO)</strong>, offering complete, end-to-end solutions for outsourced Payroll
                        administration, Human Resources, Benefits, Risk Management and Workers’ Compensation services for businesses of any size, anywhere in the country.</p>
                    <p>Other <strong>PEOs</strong> may tell you that they can help you grow your business…but how many are 100% committed to your success? Only one <strong>PEO services
                        provider</strong> is in that category: <strong>VensureHR</strong>.</p>
                    <p>As an experienced <strong>PEO</strong>, we will give you unlimited access to tools and resources that can minimize or completely remove your daily
                        administrative and management burden. By streamlining essential business tasks and minimizing threats to your business, we provide cost-effective,
                        high-quality employee benefit solutions and reduced workers’ compensation-associated costs.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
        <div class="row text-center">
            <div class="col-lg-2-5">
                <a href="<?php echo basePathUrl();?>peo-services/payroll-services" class="peo-services">
                    <img src="<?php echo basePathUrl();?>images/home/icons/Vensure-HR-Payroll-S.png">
                    <p class="m-t-10">Payroll</br>Administration</p>
                </a>
            </div>
            <div class="col-lg-2-5">
                <a href="<?php echo basePathUrl();?>peo-services/human-resources" class="peo-services">
                    <img src="<?php echo basePathUrl();?>images/home/icons/Vensure-HR-Human-Resources-Icon-S.png">
                    <p class="m-t-10">Human</br>Resources</p>
                </a>
            </div>
            <div class="col-lg-2-5">
                <a href="<?php echo basePathUrl();?>peo-services/employee-benefits" class="peo-services">
                    <img src="<?php echo basePathUrl();?>images/home/icons/Vensure-HR-Health-Benefits-S.png">
                    <p class="m-t-10">Employee</br>Benefits</p>
                </a>
            </div>
            <div class="col-lg-2-5">
                <a href="<?php echo basePathUrl();?>peo-services/risk-management" class="peo-services">
                    <img src="<?php echo basePathUrl();?>images/home/icons/Vensure-HR-Risk-Safety-S.png">
                    <p class="m-t-10">Risk</br>Management</p>
                </a>
            </div>
            <div class="col-lg-2-5">
                <a href="<?php echo basePathUrl();?>peo-services/workers-compensation" class="peo-services">
                    <img src="<?php echo basePathUrl();?>images/home/icons/Vensure-HR-Workers-Comp-S.png">
                    <p class="m-t-10">Workers'</br>Compensation</p>
                </a>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Flexible Business Solutions with a PEO</h4>
                    <div class="inside-spacer"></div>
                    <p>Each business is unique. From operational differences to varying business objectives, VensureHR has the ability to design a flexible, tailored
                        solution to satisfy even your most complex company mission.</p>
                    <p>Throughout your new PEO partnership, you maintain control of your business but with the added benefit of anytime access to some of the top HR,
                        compliance, and payroll specialists in the industry. Our business is to help improve yours—never fear your internal HR team will be replaced.
                        Instead, you can consider the VensureHR team as an extension of your own in-house team of professionals.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/VensureHR-Flexible-Business.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-50"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Industries We Support</h4>
            </div>
        </div>
        <div class="section-spacer-20"></div>
        <div class="row text-center">
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-utensils"></i></div>
                    <h3>Restaurant</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-building"></i></div>
                    <h3>Construction</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-graduation-cap"></i></div>
                    <h3>Education</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-university"></i></div>
                    <h3>Government</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-user-plus"></i></div>
                    <h3>Staffing</h3>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-wrench"></i></div>
                    <h3>Manufacturing</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-money-bill-alt"></i></div>
                    <h3>Financial Services</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-shopping-bag"></i></div>
                    <h3>Retail</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-medkit"></i></div>
                    <h3>Healthcare</h3>
                </div>
            </div>
            <div class="col-lg-2-5">
                <div class="icon-box large center">
                    <div class="icon"><i class="fa fa-heart"></i></div>
                    <h3>Nonprofit</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/videos/Vensure-HR-Total-Staffing.jpg" alt=""></a>
                        </div>
                        <div class="portfolio-description">
                            <a data-lightbox="iframe" href="https://vimeo.com/217736209?autoplay=1">
                                <i class="fas fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>More Time to Focus on Your Business</h4>
                <div class="inside-spacer"></div>
                <p><With thousands of clients around the country, <strong>VensureHR</strong> has changed the way clients manage people, retain talent, accomplish goals, and improve company culture.
                         Instead of spending weeks preparing for annual enrollment, auditing your employee handbook, researching updates to federal, state, and local compliance laws,
                         you can focus on building your business, tending to your customers, and giving your employees the attention they deserve.</p>
                <p>Regain organizational peace-of-mind with the technology and industry best practices to accelerate your business through a
                    <strong>PEO partnership—VensureHR</strong> will take it from here.</p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR and PEO solutions to Simplify Your Day</h4>
                <p class="m-t-40 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-20"></div>
</section>
