<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/RiskMgmtandCompl.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Risk Management & Compliance</h1>
            <span>Safety & Support for Your Workforce</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Risk-Management-Analyzing-Risks.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Analyzing Risks</h4>
                    <div class="inside-spacer"></div>
                    <p>The process and strategy of analyzing risks takes place long before any of your employees have an accident.
                        Implementing a proper risk and safety program can help keep employees safe, policies and procedures up-to-date, and protect your bottom line.</p>
                    <ul class="list-icon dots1 list-icon-list list-icon-colored m-l-10">
                        <li>Accident Investigation</li>
                        <li>Modified/Light Duty Return-to-Work Programs</li>
                        <li>Safety Program Development</li>
                        <li>OSHA Training and Compliance</li>
                        <li>On-Site Hazard Assessment</li>
                        <li>Worksite Hazard Training</li>
                        <li>Quarterly Review of Loss Runs</li>
                        <li>Job Task Analysis</li>
                    </ul>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h4>Loss Control and Prevention</h4>
                <div class="section-spacer-10"></div>
                <p>Every seven seconds a worker is injured on the job, according to the National Safety Council.
                    The VensureHR Loss Control team works directly with you to develop an injury and illness prevention program.
                    Management level training is also provided along the way to ensure your team has the tools necessary to provide a safe working environment.</p>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Risk-Management-Loss-Control.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Risk-Management-OSHA-Training.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>OSHA Compliance</h4>
                <div class="section-spacer-10"></div>
                <p>VensureHR’s OSHA compliance assistance services, including regularly scheduled training classes across the country, were designed to bring your
                    company’s facility and operations into compliance with federal and state laws.</p>
                <p class="m-t-30 text-center">
                    <a href="<?php echo basePathUrl();?>resources/construction-training" class="btn btn-rounded btn-light">Construction Training</a>
                </p>
                <p class="m-t-30 text-center">
                    <a href="<?php echo basePathUrl();?>resources/general-industry-training" class="btn btn-rounded btn-light">General Industry Training</a>
                </p>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR Solutions That Deliver You the Freedom to Succeed</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
</section>