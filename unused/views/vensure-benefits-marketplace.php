<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Marketplace.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Vensure Benefits Marketplace</h1>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p>The Vensure Benefits Marketplace turns your organization’s benefits offerings into more of what employees are looking for: an individualized experience.
                    Give employees the ability to make informed decisions when it comes to their benefit plans. We’ve done the legwork in order to bring employees a range of
                    intimidation-free options. A complimentary preference for any diverse and modern workforce.</p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs tabs-vertical">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="nav flex-column nav-tabs" id="myTab4" role="tablist" aria-orientation="vertical">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="home" aria-selected="true">Identity Theft Protection</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="profile" aria-selected="false">Telehealth</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="contact" aria-selected="false">Employee Paycards</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="contact" aria-selected="false">Pet Insurance Program</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab5" role="tab" aria-controls="contact" aria-selected="false">Proactive Health Management</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab6" role="tab" aria-controls="contact" aria-selected="false">Nearshore Employment Solution</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab7" role="tab" aria-controls="contact" aria-selected="false">Cross-Border Benefits</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#tab8" role="tab" aria-controls="contact" aria-selected="false">Paychecks on Demand</a>
                                </li>
                            </ul>
                            <div class="marketplace-extra-border"></div>
                        </div>
                        <div class="col-md-9 p-l-1 marketplace-bottom-space">
                            <div class="tab-content" id="tabContent">
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="home-tab">
                                    <a href="https://go.vensure.com/CoreID" target="_blank">
                                        <img src="<?php echo basePathUrl();?>images/marketplace/Core-ID-Services.jpg" />
                                    </a>
                                    <div class="inside-spacer"></div>
                                    <p>Core ID Services, LLC was created to provide real help for identity theft through sophisticated fraud detection capabilities and the fastest,
                                        most thorough recovery process. Our suite of services provides the most innovative and customer-centric identity theft solutions in the market today.
                                        Our mission is to maintain the highest standards of integrity and customer service, while offering solutions that meet or exceed industry best practices,
                                        to provide the best identity theft solutions in the industry.</p>
                                    <h4>Real Help for Identify Theft</h4>
                                    <p>Core ID Services offers personal ID theft protection and recovery services through group protection plans, partnerships, and to individual consumers.</p>
                                    <p>Whether employees are looking to enhance benefit plans or loyalty packages, add fraud protection, or safeguard sensitive data, Core ID Services has
                                        the technology to create a customized solution.</p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="core_id_services">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="profile-tab">
                                    <a href="https://www.mdlive.com" target="_blank">
                                        <img src="<?php echo basePathUrl();?>images/marketplace/Marketplace-Vensure-MD-Live-Logo.png" />
                                    </a>
                                    <div class="inside-spacer"></div>
                                    <h4>Be on Your Way to Feeling Better with MDLIVE</h4>
                                    <p>Healthcare should be simple, fast, and uncomplicated. MDLIVE makes it easy to visit a doctor in minutes through our mobile app, online, or by phone.
                                        Get access to quality healthcare without ever leaving your home, your job, or wherever you are. Find out how easy it is to receive affordable,
                                        quality care without traveling to the doctor’s office!</p>
                                    <h4>Quality Healthcare Starts With Quality Doctors</h4>
                                    <p>MDLIVE’s friendly, board-certified doctors (averaging 15-years experience) are revolutionizing remote access to quality healthcare and are trained
                                        to use virtual technology to treat many non-emergency conditions.</p>
                                    <p>Whether employees are looking to enhance benefit plans or loyalty packages, add fraud protection, or safeguard sensitive data, Core ID Services has
                                        the technology to create a customized solution.</p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="md_live">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="contact-tab">
                                    <a href="https://www.firstdata.com/moneynetwork/index.html" target="_blank">
                                        <img src="<?php echo basePathUrl();?>images/marketplace/Marketplace-Vensure-Money-Network-Logo.png" />
                                    </a>
                                    <div class="inside-spacer"></div>
                                    <p>The Money Network Service gives employees the freedom and flexibility needed to access and manage wages on-demand.</p>
                                    <h4>Employees Want Payroll Cards</h4>
                                    <p><strong>82%</strong> Say working for a company that offers payroll card is a major benefit</p>
                                    <p><strong>76%</strong> Like an employer more for offering a payroll card</p>
                                    <p><strong>73%</strong> Would look for employer that offers payroll card, when looking for next job</p>
                                    <p><strong>88%</strong> Say payroll cards save time vs having to cash a check</p>
                                    <p class="text-small">Source: Payroll Card survey study was commissioned by Visa and conducted by independent quantitative research firm Ipsos.
                                        Loyalty among 815 payroll card owners in the US in May 2017</p>
                                    <div class="line"></div>
                                    <h4>The Money Network® Service can help</h4>
                                    <p>Simplify Your Payroll Process and Provide Employees with Choice. You’ll have the ability to achieve 100% electronic pay, thus
                                        reducing the costs and administration associated with issuing paper checks.</p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="money_networks">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="contact-tab">
                                    <a href="https://www.petinsurance.com/" target="_blank">
                                        <img src="<?php echo basePathUrl();?>images/marketplace/nationwide-logo-vector.png" />
                                    </a>
                                    <div class="inside-spacer"></div>
                                    <h4>Pet Insurance Program Highlights</h4>
                                    <p>
                                        <ul class="marketplace-list">
                                            <li>All pets are welcome (dogs, cats, birds, rabbits, ferrets, reptiles, and exotic pets)</li>
                                            <li>Visit any vet, anywhere</li>
                                            <li>No minimum participation requirement</li>
                                            <li>Easy administration</li>
                                            <li>Employee enroll online or by phone</li>
                                        </ul>
                                    </p>
                                    <h4>Coverage</h4>
                                    <p>Employees choose between My Pet Protection or My Pet Protection with Wellness</p>
                                    <h4>My Pet Protection</h4>
                                    <p>Pays 90% of the veterinarian invoice for all medical expenses after a $250 annual deductible.</p>
                                    <p>
                                    <ul class="marketplace-list">
                                        <li>Accidents</li>
                                        <li>Injuries</li>
                                        <li>Common Illnesses</li>
                                        <li>Serious Illnesses</li>
                                        <li>Surgeries and hospitalizations</li>
                                        <li>X-rays, MRIs, and CT scans</li>
                                        <li>Prescription medications, chemotherapy, and therapeutic diets</li>
                                    </ul>
                                    </p>
                                    <h4>Plus</h4>
                                    <p>
                                    <ul class="marketplace-list">
                                        <li>24/7 Vet helpline</li>
                                        <li>Boarding/kennel fees if a family member is hospitalized</li>
                                        <li>Advertising/reward fees to find a lost pet</li>
                                        <li>Pet replacement cost if pet goes missing</li>
                                        <li>Mortality coverage for euthanization, cremation, and burial</li>
                                    </ul>
                                    </p>
                                    <h4>My Pet Protection with Wellness</h4>
                                    <p>Pays 90% of the veterinarian invoice for all medical expenses after a $250 annual deductible.</p>
                                    <p>
                                    <ul class="marketplace-list">
                                        <li>My Pet Protection + Wellness Preventative Health such as:</li>
                                        <li>Wellness exams</li>
                                        <li>Dental cleaning</li>
                                        <li>Vaccinations</li>
                                        <li>Spay/neuter</li>
                                        <li>Flea and tick prevention medication</li>
                                        <li>Heartworm testing and prevention medication</li>
                                        <li>Routine blood tests</li>
                                    </ul>
                                    </p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="nationwide">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="contact-tab">
                                    <a href="https://thephmp.com" target="_blank">
                                        <img src="<?php echo basePathUrl();?>images/marketplace/Marketplace-Vensure-PHMP-Logo.png" />
                                    </a>
                                    <div class="inside-spacer"></div>
                                    <p>The Proactive Health Management Plan (PHMP) is designed to encourage employees to use preventive care to create healthy living habits
                                        and to identify early potential medical issues.</p>
                                    <p>A supplemental health benefits package, PHMP aims to get employees in control of their well-being.
                                        From online health coaching to 24/7 access to medical professionals through the telehealth program, and more.
                                        On average*, employees enrolled in PHMP see an increase in each paycheck. After enrollment is complete,
                                        the employee will receive an email and a phone call to review enrollment details, determine the potential paycheck increase,
                                        and answer any questions regarding the many benefits and savings of PHMP.</p>
                                    <div class="line"></div>
                                    <h4>Employee Health is Invaluable</h4>
                                    <p>Employees lead busy lives and are often too busy to stop and focus on their own health. PHMP offers the expertise and encouragement
                                        needed to get employees health back on track. PHMP will work with employees to create a new normal, work to prevent, diminish,
                                        or in some cases, eliminate impending health issues altogether.
                                        Improved employee health makes for a stronger and empowered workforce.</p>
                                    <h4>Renewed Health Focus</h4>
                                    <p>PHMP is a limited benefit health insurance plan providing covered members with fully insured indemnity benefits including health and
                                        lifestyle coaches specializing in personal training and life coaching. Reinforce behavioral changes by making better choices,
                                        starting with adding PHMP to your benefits plan.</p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="proactive_health_management_plan">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="tab6" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <a href="https://solvoglobal.com" target="_blank">
                                                <img src="<?php echo basePathUrl();?>images/marketplace/Marketplace-Vensure-Solvo-Logo.png" />
                                            </a>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="grid-item">
                                                <div class="grid-item-wrap">
                                                    <div class="grid-image"> <img alt="Image Lightbox" src="<?php echo basePathUrl();?>images/marketplace/Solvo-Intro-Thumbnail.jpg" /> </div>
                                                    <div class="grid-description">
                                                        <a href="https://vimeo.com/301057548" data-lightbox="iframe"><i class="icon-play"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>A nearshore service provider for US companies who are looking to outsource business functions or transfer employees to a less expensive
                                        region near their headquarters. Bolster your organization with highly qualified bilingual personnel all while improving profitability.</p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="solvo">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="tab7" role="tabpanel" aria-labelledby="contact-tab">
                                    <a href="https://go.vensure.com/CoreID" target="_blank">
                                        <img src="<?php echo basePathUrl();?>images/marketplace/VES-small-logo.jpg" width="300" height="94" />
                                    </a>
                                    <div class="inside-spacer"></div>
                                    <h4>Provide Coverage for Your Loved Ones, Across All Borders</h4>
                                    <p>Cross-Border Benefits is a VensureHR provided program that offers US based employees a secure way to transfer funds and pay for
                                        healthcare benefits with family members or friends in Mexico. Without compromising the integrity of healthcare services provided,
                                        Cross-Border Benefits creates an affordable alternative to healthcare services across the US-Mexico border for individuals and families.</p>
                                    <h4>Cross-Border Benefits Advantages</h4>
                                    <p>
                                    <ul class="marketplace-list">
                                        <li>Mobile Accessibility</li>
                                        <li>Simple Processes</li>
                                        <li>Reduced Rates</li>
                                        <li>Immediate and Secure Transfers</li>
                                        <li>Reliable, Safe Transactions</li>
                                        <li>Cross-Border Benefits provides</li>
                                    </ul>
                                    </p>
                                    <p>Cross-Border Benefits provides a comprehensive network of healthcare providers and services that allow employees to assist their loved ones residing abroad.
                                        For just a few dollars each pay period, employees are able to pay for their healthcare benefits with family members and friends.
                                        For a minimal fee, employees are able to provide immediate health benefit coverage to their families.</p>
                                    <h4>Program Benefits</h4>
                                    <p>VensureHR’s Cross-Border Benefits provides expansive coverage for individuals and families, which includes no denial of preexisting
                                        conditions, no cap on funds, and no requirements for dependents and beneficiaries.</p>
                                    <p>
                                        <ul class="marketplace-list">
                                            <li>Preventative and Emergency Care (Individual and Family)</li>
                                            <li>Family Coverage</li>
                                            <li>Minor Health</li>
                                            <li>Major Health  (Requires medical underwriting for proof of insurability.)</li>
                                        </ul>
                                    </p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="vensure_employer_service">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="tab8" role="tabpanel" aria-labelledby="contact-tab">
                                    <a href="https://zayzoon.com/" target="_blank">
                                        <img src="<?php echo basePathUrl();?>images/marketplace/Marketplace-Vensure-ZayZoon-Logo.png" />
                                    </a>
                                    <div class="inside-spacer"></div>
                                    <h4>Your Paycheck On-Demand</h4>
                                    <p>Access your paycheck before your payday and forget about late bill payments and over draft fees. Get paid on your schedule and terms</p>
                                    <h4>No Debt</h4>
                                    <p>Cash in your hours when you need them. ZayZoon’s payroll-integrated service means that you can access your hours before payday,
                                        never taking on any debt in the process.</p>
                                    <h4>Easy to Use</h4>
                                    <p>It’s a two-step process: choose how much money you’d like to access and click submit.</p>
                                    <p>Don’t worry about remembering when to pay us back. Funds are automatically debited from your account on the next payday.</p>
                                    <div class="line"></div>
                                    <form class="form-marketplace" novalidate action="<?php echo basePathUrl();?>form-send/marketplace" role="form" method="post">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="full_name" class="form-control form-input-home" placeholder="Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" name="company_email" class="form-control form-input-home email" placeholder="Company Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="phone_number" class="form-control form-input-home" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="company" class="form-control form-input-home" placeholder="Company" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" name="number_employees" class="form-control form-input-home" placeholder="Number of Employees" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <textarea name="message" class="form-control form-input-home" placeholder="Your Message" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="marketplace_company" value="zay_zoon">
                                                <button type="submit" id="marketplace-bottom" class="btn btn-rounded btn-light">Send</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
