<section id="page-title" class="internals"
    data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/parallax-about.jpeg">
    <div class="container">
        <div class="page-title">
            <h1 class="text-white" style="text-shadow: 0px 0px 30px rgba(0, 0, 0, 1);">One Team, One Mission
            </h1>
            <div class="separator  small center  "
                style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
    </div>

</section>

<section>
    <div class="container">
        <div class="heading-text heading-section text-center">
            <div class="section-spacer-10"></div>
            <h4>About Us</h4>
            <div class="separator  small center  "
                style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div>
                    <p class="text-center">Our in-house team of
                        certified payroll professionals applied
                        nearly 120 years of collective experience
                        in multi-state payrolls, human resources,
                        customer service, accounting, and payroll
                        taxes to develop EmployeeMax, the leader
                        for secure cloud-based payroll and HR
                        services.</p>
                    <div class="inside-spacer"></div>
                    <div class="col-lg-8" style="margin: 0 auto;">
                        <div class="portfolio-item drop-shadow">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/about-video.png"
                                            alt=" "></a>
                                </div>
                                <div class="portfolio-description">
                                    <a data-lightbox="iframe" href="https://vimeo.com/389605367?autoplay=1">
                                        <i class="fas fa-play"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="orange">
    <div class="container">
        <div class="heading-text heading-section text-center">
            <div class="section-spacer-10"></div>
            <h4 class="text-white">Our Goals</h4>
            <div class="separator  small center  "
                style="margin-top: 16px;margin-bottom: 16px;background-color: white;height: 3px;width: 64px;"></div>
        </div>
        <div class="col-lg-12 text-center text-white">
            <p>With our extensive experience with
                other payroll and HR systems and
                software, we aim to develop a
                platform to achieve four key goals:</p>
            <div class="section-spacer-10"></div>
            <div class="section-spacer-10"></div>
            <div class="d-flex flex-wrap">
                <div class="col-lg-3">
                    <img src="<?php echo basePathUrl();?>images/employeemax/payroll-white.png">
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>Reinvent Payroll Processes</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
                <div class="col-lg-3">
                    <img src="<?php echo basePathUrl();?>images/employeemax/users.png">
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>Simplify Customer Experience</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
                <div class="col-lg-3">
                    <img src="<?php echo basePathUrl();?>images/employeemax/secure.png">
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>Deliver Secure Access</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
                <div class="col-lg-3">
                    <img src="<?php echo basePathUrl();?>images/employeemax/product.png">
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>Reliable Products and Services</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
            </div>
            <div class="section-spacer-10"></div>
        </div>
    </div>
</section>

<section class="background-gray">
    <div class="container">
        <div class="heading-text heading-section text-center">
            <div class="section-spacer-10"></div>
            <h4>Seven Advantages of EmployeeMax</h4>
            <div class="separator  small center  "
                style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="section-spacer-10"></div>
                <div class="tabs">
                    <div class="tab">
                        <button class="tablinks" onclick="openTabs(event, 'one-tab')" id="defaultOpen"><b>1.</b>
                            <p>Premium Service</p>
                        </button>
                        <button class="tablinks" onclick="openTabs(event, 'two-tab')"><b>2.</b>
                            <p>Savings & Simplified Billing</p>
                        </button>
                        <button class="tablinks" onclick="openTabs(event, 'three-tab')"><b>3.</b>
                            <p>Easy Use, Easy Access</p>
                        </button>
                        <button class="tablinks" onclick="openTabs(event, 'four-tab')"><b>4.</b>
                            <p>Custom Reporting</p>
                        </button>
                        <button class="tablinks" onclick="openTabs(event, 'five-tab')"><b>5.</b>
                            <p>Comprehensive Payroll Tax Services</p>
                        </button>
                        <button class="tablinks" onclick="openTabs(event, 'six-tab')"><b>6.</b>
                            <p>Seamless Integration</p>
                        </button>
                        <button class="tablinks" onclick="openTabs(event, 'seven-tab')"><b>7.</b>
                            <p>Security for Your Needs</p>
                        </button>
                    </div>

                    <div id="one-tab" class="tabcontent">
                        <div class="title d-flex align-items-center">
                            <p>1</p>
                            <h3>Premium Service</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>You receive informed, friendly,
                                    one-on-one customer service from
                                    our in-house team of Certified
                                    Payroll Professional (CPP) experts.
                                    They are well positioned to answer
                                    your questions quickly and effectively,
                                    since they are also the very people
                                    who designed and built the software!</p>
                                <div class="section-spacer-10"></div>
                            </div>
                            <div class="col-lg-7" style="margin: 0 auto">
                                <div class="portfolio-item drop-shadow">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img
                                                    src="<?php echo basePathUrl();?>images/employeemax/premium-service-image.jpg"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section-spacer-10"></div>

                    </div>
                    <div id="two-tab" class="tabcontent">
                        <div class="title d-flex align-items-center">
                            <p>2</p>
                            <h3>Savings & Simplified Billing</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>You will enjoy significant annual
                                    savings over your previous provider,
                                    while never having to worry about a
                                    surprise bill or hidden charge. That’s
                                    right — you can rely on inclusive monthly
                                    billing with ESO. No extra Year-End or
                                    Quarterly Processing fees. One bill, one
                                    flat monthly rate, with everything included.
                                    Period.</p>
                                <div class="section-spacer-10"></div>
                            </div>
                            <div class="col-lg-7" style="margin: 0 auto">
                                <div class="portfolio-item drop-shadow">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img
                                                    src="<?php echo basePathUrl();?>images/employeemax/save.jpeg"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="three-tab" class="tabcontent">
                        <div class="title d-flex align-items-center">
                            <p>3</p>
                            <h3>Easy Use, Easy Access</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Our Internet-based EmployeeMax software
                                    was designed with simplicity in mind. Its
                                    straightforward interface and flexibility
                                    make it very easy to learn. We also safely
                                    and securely take care of your conversion
                                    from your old system — including employee
                                    data, year-to-dates, tax filing accounts,
                                    direct deposits, and parallel payrolls — so
                                    you don’t have to worry. Then you can enjoy
                                    the flexibility of viewing and printing your
                                    data anytime, anywhere you have Internet
                                    access. On top of it all, free training,
                                    demos, webinars, and expert customer
                                    service are at your disposal!</p>
                                <div class="section-spacer-10"></div>
                            </div>
                            <div class="col-lg-7" style="margin: 0 auto">
                                <div class="portfolio-item drop-shadow">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img
                                                    src="<?php echo basePathUrl();?>images/employeemax/easy-access.jpeg"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="four-tab" class="tabcontent">
                        <div class="title d-flex align-items-center">
                            <p>4</p>
                            <h3>Custom Reporting</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>We’ve been doing this long enough
                                    to know that every business is unique
                                    in just about every way, so we don’t
                                    believe in one-size-fits-all reporting
                                    solutions. We create all of your
                                    business’s payroll reports according
                                    to your individualized needs. Along with
                                    all your other data, you can view and
                                    print these reports anytime, anywhere
                                    you have Internet access.</p>
                                <div class="section-spacer-10"></div>
                            </div>
                            <div class="col-lg-7" style="margin: 0 auto">
                                <div class="portfolio-item drop-shadow">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img
                                                    src="<?php echo basePathUrl();?>images/employeemax/report.jpeg"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="five-tab" class="tabcontent">
                        <div class="title d-flex align-items-center">
                            <p>5</p>
                            <h3>Comprehensive Payroll Tax Services</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Your days of worrying about payroll
                                    taxes are over. Our in-house team of
                                    Certified Payroll Professionals (CPP)
                                    takes care of everything. We prepare
                                    and file all state and federal payroll
                                    taxes to the designated agencies and
                                    send you copies on a quarterly basis.
                                    We file all of your W2s, W3s and annual
                                    940s, and we even produce your 1099 forms
                                    for you to file. If an agency ever contacts
                                    you with a tax notice, don’t lose sleep!
                                    Just forward the document to us and go
                                    back to running your business.</p>
                                <div class="section-spacer-10"></div>
                            </div>
                            <div class="col-lg-7" style="margin: 0 auto">
                                <div class="portfolio-item drop-shadow">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img
                                                    src="<?php echo basePathUrl();?>images/employeemax/taxes.jpeg"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="six-tab" class="tabcontent">
                        <div class="title d-flex align-items-center">
                            <p>6</p>
                            <h3>Free, Seamless Integration</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Our system is feature-packed to fully
                                    integrate with your financial software,
                                    time/attendance systems, and all other
                                    aspects of your business. This means less
                                    time, effort and money dedicated to
                                    researching, selecting, and purchasing
                                    separate modules for feature-by-feature
                                    integration. With EmployeeMax, integration
                                    is always free and seamless.</p>
                                <div class="section-spacer-10"></div>
                            </div>
                            <div class="col-lg-7" style="margin: 0 auto">
                                <div class="portfolio-item drop-shadow">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img
                                                    src="<?php echo basePathUrl();?>images/employeemax/integration-image.jpeg"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="seven-tab" class="tabcontent">
                        <div class="title d-flex align-items-center">
                            <p>7</p>
                            <h3>Security for Your Needs</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Just like everything else in your
                                    EmployeeMax package, your security needs
                                    are customized. We give you limited access
                                    to fields for as many users as you like
                                    according to your individual security needs.
                                    At the same time, we put your data on a secure
                                    network so you never have to worry about creating
                                    your own backup or losing information.</p>
                                <div class="section-spacer-10"></div>
                            </div>
                            <div class="col-lg-7" style="margin: 0 auto">
                                <div class="portfolio-item drop-shadow">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img
                                                    src="<?php echo basePathUrl();?>images/employeemax/security.jpeg"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            
        </div>
        <div class="section-spacer-10"></div>
        <div class="section-spacer-10"></div>
    </div>
</section>

<section>
    <div class="container">
        <div class="heading-text heading-section text-center">
            <div class="section-spacer-10"></div>
            <h4>Your Satisfaction is Our Success</h4>
            <div class="separator  small center  "
                style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="text-center">Our main focus is to satisfy our customers, that is why the products we offer are:</p>
                <div class="section-spacer-10"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 d-flex justify-content-center flex-wrap">
                <div class="card-square pt-11">
                    <img class="header-image-card" src="<?php echo basePathUrl();?>images/employeemax/simpler-image.png">
                    <h5 class="title-card">
                        <a href="<?php echo basePathUrl();?>industry/restaurant"><font class="orange-text"><b>Simpler</b></font></a>
                    </h5>
                    <p class="font-size-15">Simplicity does a 
                        lot more than make a system more 
                        user-friendly, it also makes it 
                        more valuable. </p>
                    <p class="font-size-15">EmployeeMax delivers 
                        outsourced payroll and HR solutions 
                        for companies across the United States.</p>
                    <p id="more-1" class="more font-size-15">To fulfill the expectations of our clients, 
                        we took a hard look at the software 
                        solutions we’d been working with for 
                        nearly two decades. All of them required 
                        modules and extensions and the costs that 
                        come with them — just to deliver what we 
                        considered an “essential core” for modern 
                        Payroll and HR needs.</p>
                    <button id="btn-more-1" class="btn-more" onclick="seeMore(1)">More</button>
                    <button id="btn-less-1" class="btn-less" onclick="less(1)">Less</button>
                </div>
            
                <div class="card-square pt-11">
                    <img class="header-image-card" src="<?php echo basePathUrl();?>images/employeemax/sampler-image.png">
                    <h5 class="title-card">
                        <a href="<?php echo basePathUrl();?>industry/restaurant"><font class="orange-text"><b>Smarter</b></font></a>
                    </h5>
                    <p class="font-size-15">Your satisfaction 
                        is our success. Let us make you an 
                        EmployeeMax guru! Enjoy access to 
                        EmployeeMax University where you 
                        will get smarter and more efficient 
                        about how you manage Payroll and HR. </p>
                        <p id="more-2" class="more font-size-15">Twice monthly, 
                            we offer free ongoing education on our 
                            system. We believe the more you learn, 
                            the more functionality you gain. We 
                            offer two classes twice a month: 
                            “Report Writer” and “HR and Benefits.”<br><br>
                            We aim to deliver smart, simple solutions 
                            that set the standard for the future of 
                            payroll outsourcing and enable our clients 
                            to manage their businesses more effectively.</p>
                    <button id="btn-more-2" class="btn-more" onclick="seeMore(2)">More</button>
                    <button id="btn-less-2" class="btn-less" onclick="less(2)">Less</button>
                </div>
            
                <div class="card-square pt-11">
                    <img class="header-image-card" src="<?php echo basePathUrl();?>images/employeemax/accurate-image.png">
                    <h5 class="title-card">
                        <a href="<?php echo basePathUrl();?>industry/restaurant"><font class="orange-text"><b>Accurate</b></font></a>
                    </h5>
                    <p class="font-size-15">At EmployeeMax, we 
                        aim to create tools that deliver precision 
                        and accuracy to your payroll, tax 
                        reporting and HR assets. Our tools will 
                        make your job easier and give you total 
                        confidence —everyday. </p>
                    <p id="more-3" class="more font-size-15">Based on our deep experience with other 
                        systems and software, we set out to 
                        build a platform that achieved four 
                        key goals:<br><br>
                        1. Reinvent Payroll Software <br>
                        2. Simplify Customer Experience <br>
                        3. Deliver Connected and Secure Access <br>
                        4. Provide Rock Solid Reliability </p>
                    <button id="btn-more-3" class="btn-more" onclick="seeMore(3)">More</button>
                    <button id="btn-less-3" class="btn-less" onclick="less(3)">Less</button>
                </div>
            </div>
        </div>
    </div>
</section>
