<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/benefits-parallax.jpeg">
    <div class="container">
        <div class="page-title">
            <h1 class="text-white" style="text-shadow: 0px 0px 30px rgba(0, 0, 0, 1);">Employee Benefits</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
    </div>
</section>

<section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Promote Employee Health and Wellness with Unique Benefits</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <p class="text-center">While the access to employee benefits 
                            show lower rates in 2019, those who had access to benefits 
                            showed a significant participation rate. </p>
                        <div class="inside-spacer"></div>
                        <h4 class="text-center">Chart 1. Employeer-sponserd benefits: Access, participation and take-up 
                            rates, March 2019
                        </h4>
                        <div class="section-spacer-10"></div>
                        <div class="inside-spacer"></div>
                        <div class="chart-container">
                            <div class="numbers">
                                <div class="percent"><span>100%</span></div>
                                <div class="percent"><span>90%</span></div>
                                <div class="percent"><span>80%</span></div>
                                <div class="percent"><span>70%</span></div>
                                <div class="percent"><span>60%</span></div>
                                <div class="percent"><span>50%</span></div>
                                <div class="percent"><span>40%</span></div>
                                <div class="percent"><span>30%</span></div>
                                <div class="percent"><span>10%</span></div>
                                <div class="percent"><span>20%</span></div>
                                <div class="percent"><span>0%</span></div>
                            </div>
                            <div id="chart">
                            <ul id="bars">
                                <li>
                                    <div class="vre">
                                    <div class="group">
                                        <div data-percentage="70" class="bar b"></div>
                                        <div data-percentage="67" class="bar bd"></div>
                                        <div data-percentage="91" class="bar r"></div>
                                        <span>Access</span>
                                    </div>
                                    <div class="group">
                                        <div data-percentage="55" class="bar b"></div>
                                        <div data-percentage="52" class="bar bd"></div>
                                        <div data-percentage="83" class="bar r"></div>
                                        <span>Participation</span>
                                    </div>
                                    <div class="group">
                                        <div data-percentage="79" class="bar b"></div>
                                        <div data-percentage="77" class="bar bd"></div>
                                        <div data-percentage="90" class="bar r"></div>
                                        <span>Take-up rate</span>
                                    </div>
                                    </div>
                                    
                                    <p class="name-group">Retirement</p>
                                </li>
                                <li>
                                    <div class="vre">
                                    <div class="group">
                                        <div data-percentage="70" class="bar b"></div>
                                        <div data-percentage="69" class="bar bd"></div>
                                        <div data-percentage="91" class="bar r"></div>
                                        <span>Access</span>
                                    </div>
                                    <div class="group">
                                        <div data-percentage="52" class="bar b"></div>
                                        <div data-percentage="49" class="bar bd"></div>
                                        <div data-percentage="70" class="bar r"></div>
                                        <span>Participation</span>
                                    </div>
                                    <div class="group">
                                        <div data-percentage="72" class="bar b"></div>
                                        <div data-percentage="70" class="bar bd"></div>
                                        <div data-percentage="88" class="bar r"></div>
                                        <span>Take-up rate</span>
                                    </div>
                                    </div>
                                    <p class="name-group" >Medial Care</p>
                                </li>
                                <li>
                                    <div class="vre">
                                    <div class="group">
                                        <div data-percentage="60" class="bar b"></div>
                                        <div data-percentage="56" class="bar bd"></div>
                                        <div data-percentage="83" class="bar r"></div>
                                        <span>Access</span>
                                    </div>
                                    <div class="group">
                                        <div data-percentage="60" class="bar b"></div>
                                        <div data-percentage="56" class="bar bd"></div>
                                        <div data-percentage="79" class="bar r"></div>
                                        <span>Participation</span>
                                    </div>
                                    <div class="group">
                                        <div data-percentage="98" class="bar b"></div>
                                        <div data-percentage="98" class="bar bd"></div>
                                        <div data-percentage="98" class="bar r"></div>
                                        <span>Take-up rate</span>
                                    </div>
                                    </div>
                                    <p class="name-group" >Life Insurence</p>
                                </li>
                                    
                            </ul>
                        </div>
                        </div>
                        <div class="leyend">
                            <div class="b"></div>
                            <span>Civilian</span>
                            <div class="bd"></div>
                            <span>Private industry</span>
                            <div class="r"></div>
                            <span>State and local goverment</span>
                        </div>
                        <div class="inside-spacer"></div>
                        <p class="text-center"><b>Source: U.S. Department of Labor Bureau of Labor Statistics (2019)</b></p>
                        <div class="inside-spacer"></div>
                        <p>As indicated in the chart, the take-up rate – the number 
                            of individuals who had access to benefits and participated 
                            in them – remains consistently high. Not only offering 
                            employee benefits, but unique benefits that are tailored 
                            to your employees’ needs will set you apart from your 
                            competitors and provide additional benefits to you.</p>
                        <div class="inside-spacer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Top 5 Reasons to Offer Employee Benefits</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-2">
                    <div class="top" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/top-1.jpeg')">
                        <div class="top-layer"></div>
                        <div class="number">1</div>
                    </div>
                    <div class="top-title">
                        <p><b>Attract Top Talent</b></p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="top" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/top-2.jpeg')">
                        <div class="top-layer"></div>
                        <div class="number">2</div>
                    </div>
                    <div class="top-title">
                        <p><b>Lower Turnover</b></p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="top" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/top-3.jpg')">
                        <div class="top-layer"></div>
                        <div class="number">3</div>
                    </div>
                    <div class="top-title">
                        <p><b>Increase Morale</b></p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="top" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/top-4.jpeg')">
                        <div class="top-layer"></div>
                        <div class="number">4</div>
                    </div>
                    <div class="top-title">
                        <p><b>Improved Health</b></p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="top" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/top-5.jpeg')">
                        <div class="top-layer"></div>
                        <div class="number">5</div>
                    </div>
                    <div class="top-title">
                        <p><b>Higher Productivity</b></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>EmployeeMax Offers Extensive Employee Benefit Solutions</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/employee-offers-benefits.jpeg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex align-items-center">
                    <div>
                        <p class="text-justify">Whether you are exploring 
                            the differences between plan features and prices 
                            or looking for benefits that will attract 
                            employees and that employees will participate 
                            in, EmployeeMax has a wide array of benefits 
                            to choose from. We value employee well-being 
                            and utilize our long-standing partnerships 
                            and business relationships with vendors to 
                            find the best, innovative, and attractive 
                            benefits on the marketplace. We know that no 
                            benefit package is a one-size-fits-all, which 
                            is why we offer the basics, ancillary, and 
                            voluntary benefit options.</p>
                        <div class="inside-spacer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>EmployeeMax Offers Extensive Employee Benefit Solutions</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row justify-content-center ">
                <div class="col-lg-12 d-flex justify-content-center flex-wrap petsy">
                <div class="col-lg-3 text-center">
                    <img src="<?php echo basePathUrl();?>images/employeemax/contract.png">
                    <div class="inside-spacer"></div>
                    <h5><b>Nearshore Outsource Staffing</b></h5>
                </div>
                <div class="col-lg-3 text-center">
                    <img src="<?php echo basePathUrl();?>images/employeemax/financial.png">
                    <div class="inside-spacer"></div>
                    <h5><b>Financial Wellness Programs</b></h5>
                </div>
                <div class="col-lg-3 text-center">
                    <img src="<?php echo basePathUrl();?>images/employeemax/protect-id.png">
                    <div class="inside-spacer"></div>
                    <h5><b>Identity Theft and Fraud Protection</b></h5>
                </div>
                </div>
                <div class="col-lg-12 d-flex justify-content-center flex-wrap petsy">
                <div class="col-lg-3 text-center">
                    <img src="<?php echo basePathUrl();?>images/employeemax/car.png">
                    <div class="inside-spacer"></div>
                    <h5><b>Rental Car Discount Membership</b></h5>
                </div>
                <div class="col-lg-3 text-center">
                    <img src="<?php echo basePathUrl();?>images/employeemax/pet.png">
                    <div class="inside-spacer"></div>
                    <h5><b>Pet Insurance</b></h5>
                </div>
                <div class="col-lg-3 text-center">
                    <img src="<?php echo basePathUrl();?>images/employeemax/health.png">
                    <div class="inside-spacer"></div>
                    <h5><b>Cost-sharing Healthcare Plans</b></h5>
                </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="inside-spacer"></div>
                    <p class="text-justify">Don’t wait until you or your employees face an emergency 
                            circumstance and need immediate healthcare coverage. 
                            <a class="contact" href="<?php echo basePathUrl();?>contact">
                            <b>Contact EmployeeMax</b></a><a> where 
                            one of our benefits specialists can help you find an 
                            employee benefits package tailored to your business’s 
                            needs. </a></p>
                        <div class="inside-spacer"></div>
                </div>
            </div>
        </div>
    </section>