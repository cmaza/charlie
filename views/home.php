
    <section class="fullscreen home" data-bg-parallax-home="<?php echo basePathUrl();?>images/employeemax/parallax-home.jpg">
        <div class=""></div>
        <div class="container-wide">
            <div class="container-fullscreen">
                <div class="text-middle">
                    <div class="heading-text no-bottom">
                        <h1><span class="text-white" style="text-shadow: 2px 2px 5px rgba(0, 0, 0, 1);">Welcome to EmployeeMax</span></h1>
                        <p class="text-white" style="text-shadow: 2px 2px 5px rgba(0, 0, 0, 1);">Integrated PEO Solutions</p>
                        <div class="inside-spacer"></div>
                        <form class="form-free-diagnostic-home" novalidate="" action="<?php echo basePathUrl();?>form-send/free-diagnostic-home" role="form" method="post" data-success="<?php echo basePathUrl();?>landing-page/thank-you">
                            <div class="input-group form-control-lg form-control-home">
                                <input type="email" required="" name="company_email" class="form-control required email form-input-home" placeholder="Company Email Address">
                            </div>
                            <div class="input-group form-control-lg form-control-home m-t-30">
                                <button type="submit" id="free-diagnostic-home-bottom" class="btn btn-light">Schedule a FREE Demo <i class="fas fa-calendar-alt" style="margin-left: 7px;"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Outsourcing Your Payroll</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <h4>The leader for secure, cloud-based payroll and HR services.</h4>
                    <div class="inside-spacer"></div>
                    <p>
                    EmployeeMax offers a fully-integrated platform 
                    that provides the most desired functions for HR 
                    and payroll management. A seamless conversion from 
                    your current system means the business will have 
                    access to a full range of customizable features, 
                    including payroll tax reporting, hiring and 
                    onboarding, and benefits administration from 
                    a single, easy-access, self-service portal.
                    </p>
                    <div class="inside-spacer"></div>
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/offer.png" alt=""></a>
                                <div class="inside-spacer"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/offer2.jpeg" alt=""></a>
                            </div>
                        </div>
                    </div>   
                    <div class="inside-spacer"></div>
                    <div class="inside-spacer"></div>
                    <p>EmployeeMax’s service team is 
                    committed to not only making your everyday PEO, payroll, 
                    and human capital management technology experience one 
                    that stands out, but we aim to help automate procedures, 
                    remove redundant tasks, and help you manage your employees. </p>
                    <div class="inside-spacer"></div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="orange sa section-image parallax-section" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/Pittsburgh.jpg')">
       <div class="bg-layer"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 text-center ">
                <div class="opacity-square">
                    <div class="wrapper text-white">
                        <span data-number="1" class="box third-number noanimation" ></span>
                        <span data-number="2" class="box second-number noanimation" ></span>
                        <span data-number="0" class="box first-number noanimation" ></span>
                    </div>
                    <h5 class="text-white">Years Of Collective Experience</h5>
                    </div>
                </div>
                <div class="col-lg-4 d-flex justify-content-center">
                <div class="opacity-square">
                    <ul class="text-white check-list">
                        <li><h5>Multi-state Payrolls</h5></li>
                        <li><h5>Human Resources</h5></li>
                        <li><h5>Customer Service and Accounting</h5></li>
    
                    </ul>
                </div>
                    
                </div>
                
            </div>
        </div>
    </section>

    <section>
    <div class="container">
    <div class="heading-text heading-section text-center">
                <h4>Our Services</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
        <div class="row">
            <div class="row justify-content-center text-center">
                <div class="col-lg-2 card-service">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Payroll-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/restaurant"><font class="orange-text"><b>Payroll</b></font></a>
                    </h5>
                    <p class="font-size-16">Integrated, simple, 
                        and accurate payroll and tax reports, 
                        saving you money, time and effort.</p>
                    <a class="btn"><p><b>Tell me more</b></p></a>
                </div>
                <div class="col-lg-2 card-service">
                    <img src="<?php echo basePathUrl();?>images/employeemax/HR-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/healthcare"><font class="orange-text"><b>Human Resources</b></font></a>
                    </h5>
                    <p class="font-size-16">Simple, centralized, 
                    and integrated systems to eliminate technology 
                    redundancies and increase accuracy across 
                    the organization.</p>
                    <a class="btn"><p><b>Tell me more</b></p></a>
                </div>
                <div class="col-lg-2 card-service">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Benefits-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/tech"><font class="orange-text"><b>Employee Benefits</b></font></a>
                    </h5>
                    <p class="font-size-16">Extensive resources 
                        available for adequate employee benefit 
                        options.</p>
                    <a class="btn"><p><b>Tell me more</b></p></a>
                </div>
                <div class="col-lg-2 card-service">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Time-and-Attendance-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/non-profit"><font class="orange-text"><b>Time and Attendance</b></font></a>
                    </h5>
                    <p class="font-size-16">Web-based, biometric 
                        support for accurate time and labor 
                        tracking.</p>
                    <a class="btn"><p><b>Tell me more</b></p></a>
                </div>
                <div class="col-lg-2 card-service">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Workers-Comp-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/construction"><font class="orange-text"><b>Workers' Compensation</b></font></a>
                    </h5>
                    <p class="font-size-16">Affordable, 
                        pay-as-you-go programs to protect and
                        cover your employees from injury or accident.</p>
                    <a class="btn"><p><b>Tell me more</b></p></a>
                </div>
            </div>
        </div>
    </div>   
    </section>
    
    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Partner with EmployeeMax</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/partner-with.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex align-items-center">
                    <div>
                        <p>EmployeeMax was built on the premise of 
                        providing comprehensive and professional human 
                        resources, payroll, benefits, and workers’ 
                        compensation management and administration 
                        services to businesses large and small. To achieve 
                        this goal we sourced a team of seasoned experts who 
                        could best outfit each department, allowing us to 
                        provide a top technology platform to help run it all. </p>
                        <div class="inside-spacer"></div>
                        <p>We put immense value in the 
                        trusting relationships we have with business owners, 
                        which is why we put all our efforts into maximizing 
                        each client’s success through customized solutions 
                        tailored to meet the individual needs of the business, 
                        the community, and the industry.</p>
                        <div class="inside-spacer"></div>
                        <p>Partnering with EmployeeMax means you share 
                        our drive to streamline business processes so 
                        employers can achieve business goals faster. We 
                        provide the tools, resources, and support to run 
                        your business more efficiently, all while saving 
                        time and money on back-end operations, administration, 
                        and management. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-0">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Outsourcing Your Back-Office Services</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="inside-spacer"></div>
            </div>
        </div>
    </section>

    <section class="squares m-0 ">
        <div class="container">
        <div class="row m-0 p-0 justify-content-center">
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/conversion.png')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Simple Conversion and Integration</p>
                    <p>Reduce company burdens regarding 
                    HR software by empowering employees 
                    to take charge of their HR needs. </p>
                </div>
                
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/automation.png')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Complete Automation</p>
                    <p>Automate areas of your business 
                    that typically require human intervention. 
                    Payroll processing and approval, onboarding, 
                    reporting, etc.  </p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/bigbook.png')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Employee Handbook</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/timecards.jpg')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Time Tracking</p>
                    <p>Instead of wasting countless 
                    minutes helping manage employee’s 
                    hourly tracking, make the time 
                    tracking experience a pleasant 
                    experience.</p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/rh.png')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Payroll</p>
                    <p>From payroll processing and 
                    approvals to payroll tax and garnishments 
                    or unemployment claims management, 
                    we offer a one-stop-shop for it all</p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/nomina.png')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Taxes</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
            </div>
        </div>
        </div>
        <div class="section-spacer-10"></div>
    </section>
    
    <section class="background-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 heading-text heading-section">
                    <div class="section-spacer-10"></div>
                    <h4>Why Choose EmployeeMax</h4>
                    <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                    <div class="inside-spacer"></div>
                </div>
                <div class="col-lg-5">
                    <h4 class="orange-text">Let’s face it, 
                        whether you are a small startup or a major 
                        public company, your business needs payroll 
                        and HR solutions that are not going to be 
                        obsolete in 10 years.</h4>
                    <div class="inside-spacer"></div>
                    <p class="text-justify">EmployeeMax was designed 
                    to offer a fully-integrated, feature-packed software 
                    platform to provide the most desired functions for 
                    HR and payroll management, and everything in between.</p>
                    <div class="inside-spacer"></div>
                    <p class="text-justify">Complete a seamless conversion 
                    from your current human capital management software 
                    to a platform front-loaded with robust payroll tax 
                    reporting, enhanced security features, and everything 
                    your business needs in terms of hiring and onboarding.</p>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/employee-max-video-home.png" alt=" "></a>
                            </div>
                            <div class="portfolio-description">
                                <a data-lightbox="iframe" href="https://vimeo.com/389606034?autoplay=1">
                                    <i class="fas fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
