<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/parallax-hr-services.jpeg">
    <div class="container">
        <div class="page-title">
            <h1 class="text-white" style="text-shadow: 0px 0px 30px rgba(0, 0, 0, 1);">
            HR Services
            </h1>
            <h1 class=""></h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
            <h5 class="text-white" style="text-shadow: 0px 0px 30px rgba(0, 0, 0, 1);">Manage All Your HR Services with One Tool</h5>
        </div>
    </div>
</section>

<section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>HR Features</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/hr-services.jpeg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex align-items-center">
                    <div>
                        <p>EmployeeMax combined the human 
                        resource services with payroll management in a single 
                        platform to save businesses from the taxing, manual 
                        data entry that results in unnecessary human errors. 
                        Our software is fully customizable and has the ability 
                        to streamline various lines of your business, including 
                        employee benefits, time tracking, HR consulting, and 
                        pay-as-you-go workers’ compensation.</p>
                        <div class="inside-spacer"></div>
                        <p>EmployeeMax offers a variety of HR services, from employee
                               benefits and time tracking to pay-as-you-go workers’
                                compensation and HR consulting.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Human Resource Information Systems</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-5 d-flex align-items-center">
                    <div>
                        <p><b>Our system was built to provide 
                            growing businesses a turnkey solution that 
                            goes beyond payroll and HR to include benefits 
                            management.</b></p>
                        <div class="inside-spacer"></div>
                        <p>Human Resource Information Systems 
                            (HRIS) have become one of the most important tools for 
                            many businesses. Even small offices can benefit by using 
                            HRIS to become more efficient. Many firms don’t realize 
                            how much time and money is wasted on manual human resource 
                            management (HRM) tasks until they switch to an HRIS.</p>
                        <div class="inside-spacer"></div>
                        <div class="orange palete">
                            <p class="text-white m-0">HRIS are secure and web-based to 
                            provide extensive automation of all HR-related activities. 
                            EmployeeMax can offer you the most comprehensive and 
                            affordable solutions on the market today.</p>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/hr-services-page.png"
                                            alt=" "></a>
                                </div>
                                <div class="portfolio-description">
                                    <a data-lightbox="iframe" href="https://vimeo.com/389605367?autoplay=1">
                                        <i class="fas fa-play"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>

    <section class="orange">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-spacer-10"></div>
                    <p class="text-white">With EmployeeMax, you:</p>
                    <div class="section-spacer-10"></div>
                    <div class="animated" >
                        <div class="text-white animation-words">
                        <span>Have a Quick and Easy Implementation</span>
                        <span>Save Money on Hardware or Software Investments</span>
                        <span>Centralize All HR and Benefits Data Into a Single System</span>
                        <span>Receive Powerful Reporting and Exporting Capabilities</span>
                        <span>Simplify Operational Complexity and Reduce Data Collection</span>
                        <span>Automatically and Electronically Deliver Benefits Information to Each Carrier and Payroll Provider</span>
                        <span>Eliminate Manual Management Of Data Across Multiple Systems</span>
                        <span>Increase Data Accuracy And Streamline Management and Communication of Information</span>       
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </section>

    <section>
    <div class="container">
        <div class="heading-text heading-section text-center">
            <div class="section-spacer-10"></div>
            <h4>Employee Portal</h4>
            <div class="separator  small center  "
                style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                <div class="section-spacer-10"></div>
                <p>EmployeeMax’s Employee Portal makes 
                    it convenient for employees to 
                    manage their benefits and other 
                    HR-related information in real time 
                    through a central cloud-based system 
                    that is accessible anywhere. The portal is 
                    easily configurable to manage relevant 
                    features and content for the business.</p>
                <div class="section-spacer-10"></div>
                </div>
            </div>
           <div class="row">
            <div class="col-lg-12 d-flex justify-content-center employee">
                <div class="card-square pt-11">
                    <img class="header-image-card" src="<?php echo basePathUrl();?>images/employeemax/inscription.png">
                    <p class="font-size-15">Easily enroll in or change 
                    aspects of employee benefits and other HR-related 
                    information. Compare, analyze, and review costs 
                    prior to benefits enrollment.</p>
                </div>
            
                <div class="card-square pt-11">
                    <img class="header-image-card" src="<?php echo basePathUrl();?>images/employeemax/benefits.png">
                    <p class="font-size-15">View plans from different
                        benefit providers and a consolidated benefits 
                        summary report that includes the coverage employees 
                        selected with the associated costs.</p>
                </div>
            
                <div class="card-square pt-11">
                    <img class="header-image-card" src="<?php echo basePathUrl();?>images/employeemax/compensation.png">
                    <p class="font-size-15">Explore the value of employee 
                        compensation package with personalized total 
                        compensation statements. </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Industry-Leading Human Resource Solutions</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/industry.jpeg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex align-items-center">
                    <div>
                    <p>EmployeeMax, as your professional employer 
                    organization (PEO) provider, takes on a 
                    business’es day-to-day employee-related tasks. 
                    While the PEO is the employer of record on paper, 
                    the client/business remains the executive decision 
                    maker of the company. This relationship allows the 
                    business owner to focus on growing the business, 
                    while we do all the time-consuming, back-office 
                    work.</p>
                    <div class="section-spacer-10"></div>
                    <p>Combining your
                          systems into a single program saves you from manually
                           entering information into multiple systems and helps
                            eliminate potential errors.  Like the payroll system,
                             our human resource software is customizable and can
                              fully synchronize with all other aspects of your business.
                               EmployeeMax offers a variety of HR services, such
                                as time and attendance, applicant tracking, HR 
                                consulting, developing employee handbooks, and much more.</p> 
                    </div>
                </div>
            </div>
        </div>
    </section>
    