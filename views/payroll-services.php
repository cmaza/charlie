<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/parallax-payroll-services.jpeg">
    <div class="container">
        <div class="page-title">
            <h1 class="" style="">Payroll Services</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
            <h5>Direct Integrations with Intacct Creates Smoother Payroll Process</h5>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="heading-text heading-section text-center">
            <div class="section-spacer-10"></div>
            <h4>Payroll Features</h4>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
            <div class="section-spacer-10"></div>
        </div>
        <div class="col-lg-12 text-center">
            <p>EmployeeMax’s payroll software, 
                Intacct, has simplified payroll 
                processing so you can work smarter. 
                Intacct can customize requirements 
                of virtually any business, providing 
                the business with the power and 
                flexibility it deserves.</p>
            <div class="section-spacer-10"></div>
            <div class="section-spacer-10"></div>
            <div class="d-flex justify-content-center flex-wrap">
                <div class="groupa">
                    <div class="radius" style="background-image:url('<?php echo basePathUrl();?>images/employeemax/credit-card.jpg')"></div>
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>Variety of fund distribution 
                        options (direct deposit, payroll debit 
                        card, physical check)</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
                <div class="groupa">
                <div class="radius" style="background-image:url('<?php echo basePathUrl();?>images/employeemax/paycheck.jpg')"></div>
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>On-site paycheck printing</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
                <div class="groupa">
                <div class="radius" style="background-image:url('<?php echo basePathUrl();?>images/employeemax/report.jpg')"></div>
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>Employee self-service portal 
                        provides unlimited online access to paystubs,
                         W-2s, and 1099s</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
                <div class="groupa">
                <div class="radius" style="background-image:url('<?php echo basePathUrl();?>images/employeemax/payroll.jpg')"></div>
                    <div class="section-spacer-10"></div>
                    <h5 class=""><b>Preview payroll to ensure accuracy</b></h5>
                    <div class="section-spacer-10"></div>
                </div>
            </div>
            <div class="section-spacer-10"></div>
        </div>
    </div>
</section>

    <section class="background-gray si sa">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Outsourcing Your Payroll Processing Has Many Advantages</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
            <div class="col-lg-6 p-sec">
                    <div>
                        <div class="section-spacer-10"></div> 
                        <h2>Make Business More Efficient</h2>
                        <p>Continuously changing tax laws can 
                        mean harsh regulations and steep penalties 
                        for uninformed or misinformed business 
                        owners, putting even more pressure on payroll 
                        processors.</p>
                        <div class="inside-spacer"></div>
                        <p>EmployeeMax payroll experts are familiar 
                        with tax laws and are available to alleviate 
                        any concerns regarding unique payroll needs. 
                        Our team can assist businesses of any size 
                        with their payroll processing needs, so business 
                        owners can refocus their energies on running the 
                        business and growing the bottom line.</p>
                        <div class="section-spacer-10"></div>                           
                    </div>
            </div>
            <div class="col-lg-6 p-sec line-left">
                    <div class="col-lg-12 d-flex justify-content-center p-0">
                        <div class="text-center">
                            <div class="d-flex align-items-center">
                            <figure id="figure" class="chart paused" data-percent="33">
                                <figcaption>
                                    <div class="wrapper">
                                        <span data-number="3" class="box second-number noanimation"></span>
                                        <span data-number="3" class="box first-number noanimation"></span>
                                    </div>    
                                    <span style="font-size: 28px">%</span>
                                </figcaption>
                                <svg width="200" height="200">
                                    <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                                </svg>
                            </figure> 
                            <h5 class="title orange-text" style="padding-left: 1rem">FACT: 33% of Employers Make Payroll Errors</h5> 
                            </div> 
                            
                        </div>
                                        
                    </div>
                    <div class="col-lg-12 p-0">
                        <h3 class="title">Protect Yourself from Costly Payroll Errors</h3>
                        <p>The IRS estimates that one out of 
                            every three employers makes mistakes in computing 
                            payroll figures. These human errors can lead to stiff
                             financial penalties – something small and medium-sized
                              businesses just can’t afford.</p>
                        <div class="section-spacer-10"></div>                         
                    </div>
            </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Intacct</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center">Improve Company Performance Through Integrating Payroll and HR.</p>
                    <div class="section-spacer-10"></div>
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <div>
                        <div class="item">
                            <div class="icon"><img src="<?php echo basePathUrl();?>images/employeemax/real-time.png" alt=""></div>
                            <div class="text">
                                <h5 class="title">Real-time Business Visibility</h5>
                                <p>Designated staff members can access your 
                                    accounting dashboard securely from anywhere, anytime.</p>
                            </div>
                        </div>
                        <div class="section-spacer-10"></div> 
                        <div class="item">
                            <div class="icon"><img src="<?php echo basePathUrl();?>images/employeemax/financial-report.png" alt=""></div>
                            <div class="text">
                                <h5 class="title">Streamline and Simplify Reporting and Analysis</h5>
                                <p>Intacct makes financial consolidation, reporting, 
                                    and analysis easy across multiple business entities.</p>
                            </div>
                        </div>
                        <div class="section-spacer-10"></div>
                        <div class="item">
                            <div class="icon"><img src="<?php echo basePathUrl();?>images/employeemax/automate.png" alt=""></div>
                            <div class="text">
                                <h5 class="title">Automate Business Processes</h5>
                                <p>Reduce errors, eliminate routine tasks, and accelerate your closing process.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/video-intacct.png" alt=" "></a>
                            </div>
                            <div class="portfolio-description">
                                <a data-lightbox="iframe" href="https://vimeo.com/124315733?autoplay=1">
                                    <i class="fas fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Benefits of Intacct</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                            <p>EmployeeMax offers you the benefit of having us do your 1095-C
                                 and 1094-C forms. We have added a new screen for employee 
                                 data and modified the dependent screen to aid in the population
                                  of these forms. You can begin populating these screens now, 
                                  or simply wait until year-end. We will send you an Excel 
                                  spreadsheet that you can populate, and we will load the data
                                   for you!</p>
                </div>
            </div>
        </div>
    </section>

    <section class="squares m-0 ">
        <div class="container">
        <div class="row m-0 p-0 justify-content-center">
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/conversion-min.jpeg')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Simple Conversion and Integration</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
                
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/automation-min.jpeg')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Complete Automation</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/ledger-min.jpeg')">
                <div class="overlay"></div>
                <div class="information">
                    <p>General Ledger</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/timecards-min.jpeg')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Timecards</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/integrated-payroll-min.jpeg')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Integrated Payroll</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
            </div>
            <div class="col-lg-3 square" style="background-image: url('<?php echo basePathUrl();?>images/employeemax/accounting-min.jpeg')">
                <div class="overlay"></div>
                <div class="information">
                    <p>Accounting Solutions</p>
                    <p>Lorem impsum dolor aset amet
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor aset ame
                    Lorem impsum dolor</p>
                </div>
            </div>
        </div>
        </div>
        <div class="section-spacer-10"></div>
    </section>
