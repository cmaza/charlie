<section id="page-title" class="privacy-terms">
    <div class="container">
        <div class="page-title text-light">
            <h1>Terms of Use</h1>
            <div class="line-small-light"></div>
        </div>
    </div>
</section>

<section id="terms-of-use">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="under m-b-0"><strong>Terms of Use</u></strong></p>
                <p class="italic">Last updated on March 21, 2019</p>
                <p class="m-b-5"><strong>Introduction</strong></p>
                <p>Hello and welcome to this Website provided by Atlas Professional Services L.L.C., we hope you enjoy learning about us and our various offerings. Please read these terms of use (<strong>"Terms of Use"</strong>) carefully because they are a binding agreement between you and Atlas Professional Services L.L.C. ("<strong>Atlas Professional Services L.L.C."</strong>, <strong>"we"</strong>, <strong>"our"</strong> or <strong>"us"</strong>). These Terms of Use apply to your use of the Website. If you do not agree with these Terms of Use, do not access or download the Website.</p>
                <ol>
                    <li><strong>Your Use of the Website.</strong></li>
                </ol>
                <p>Atlas Professional Services L.L.C. owns and operates the Website. The documents and other information and content available on the Website is referred to as <strong>"Content."</strong> The Content is protected by copyright laws throughout the world. Atlas Professional Services L.L.C. grants you a limited, revocable license to access and use the Website and the Content. You are required to retain all copyright and other proprietary notices on any copies of the Content. Using the Website does not give you any ownership rights to the Content. Further, nothing in these Terms of Use is to be construed as conferring to you any license or right under any patent, copyright, trademark, or other intellectual property right of Atlas Professional Services L.L.C. or any third party. Atlas Professional Services L.L.C. and its suppliers reserve all rights not granted in these Terms of Use.</p>
                <p>You may not provide any Data (as defined in Section 5) or use the Website in any way that violates any laws, infringes on anyone’s rights, is offensive, or interferes with the Website or any Website features. Except as expressly provided above, you may not otherwise copy, display, download, distribute, modify, reproduce, republish or retransmit any Content or any portion of the Content in any electronic medium or in hard copy, or create any derivative work based on such Content, without our express written permission. The Website and Content are for informational purposes only and we do not make any recommendations on or via the Website; accordingly, you should not rely upon the Website or Content as the sole basis for any decision or action.</p>
                <ol start="2">
                    <li><strong>Trademarks.</strong></li>
                </ol>
                <p>All trademarks, logos and service marks (<strong>"Marks"</strong>) displayed on the Website are our property or the property of third parties. You are not permitted to use these Marks without the Marks’ owner’s prior written permission.</p>
                <ol start="3">
                    <li><strong>Modification</strong></li>
                </ol>
                <p>Atlas Professional Services L.L.C. reserves the right to (i) modify the Content or to (ii) modify, suspend, or discontinue the Website or any part the Website at any time with or without notice to you. You agree that Atlas Professional Services L.L.C. will not be liable to you or to any third party for any modification of the Content or any modification, suspension, or discontinuance of the Website.</p>
                <ol start="4">
                    <li><strong>Feedback</strong></li>
                </ol>
                <p>All information, ideas, suggestions or other communications you submit or provide to us will be non-confidential and non-proprietary ("Feedback"). Accordingly, do not submit or provide Atlas Professional Services L.L.C. with any information you consider confidential or proprietary. Unless you and Atlas Professional Services L.L.C. agree otherwise in a written agreement, Atlas Professional Services L.L.C. will be entitled to use, disclose or distribute any Feedback for any purpose whatsoever (including commercial purposes) without any obligation to you (monetary or otherwise).</p>
                <ol start="5">
                    <li><strong>User Submissions</strong></li>
                </ol>
                <p>The Website may enable you to submit emails or otherwise provide certain content, data or other information (<strong>"Data"</strong>) to Atlas Professional Services L.L.C.. You can only post Data if you own all the rights to the Data or if the owner has given you permission. You do not transfer ownership of the Data you provide, submit or post; however, by doing so, you grant Atlas Professional Services L.L.C. the irrevocable right to use, copy, modify publish, perform, transmit and display such Data in accordance with these Terms of Use, and you waive any moral rights you may have in such Data. Atlas Professional Services L.L.C. will be free to use such Data for any reason whatsoever.</p>
                <ol start="6">
                    <li><strong>Privacy Policy</strong></li>
                </ol>
                <p>Please review the Privacy Policy.</p>
                <ol start="7">
                    <li><strong>Third Party Links</strong></li>
                </ol>
                <p>The Content may contain links to websites that are owned and/or operated by third parties. Such websites are not under our control. We provide these links only as a convenience and we do not review, approve, monitor, endorse, warrant, or make any representations with respect to such websites. We are not responsible for such websites’ content or for any link(s) they may contain.</p>
                <ol start="8">
                    <li><strong>Warranty Disclaimer</strong></li>
                </ol>
                <p>ATLAS PROFESSIONAL SERVICES L.L.C. DOES NOT MAKE ANY WARRANTIES OR PROMISES ABOUT THE WEBSITE OR CONTENT. FOR EXAMPLE, INFORMATION ON THIS WEBSITE MAY NOT BE CURRENT, OR COMPLETE WHEN YOU VISIT THE WEBSITE AND IT MAY CONTAIN ERRORS AND INACCURACIES. ADDITIONALLY, WE DO NOT MAKE ANY COMMITMENTS OF THE WEBSITE LEGAILTY, AVAILABLITY, RELIABLITY OR ALBITY TO MEET YOUR NEEDS. ATLAS PROFESSIONAL SERVICES L.L.C. PROVIDES THE WEBSITE AND CONTENT "AS IS" AND FOR YOUR USE AT YOUR OWN RISK. SOME JURISDICTIONS PROVIDE CERATIN WARRANTIES, SUCH AS NONINFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE. TO THE EXTENT WE ARE PREMITTED BY LAW, ATLAS PROFESSIONAL SERVICES L.L.C. DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS IMPLIED, OR STATUTORY, INCLUDING ALL THE WARRANTIES LISTED ABOVE, AND ANY WARRANTIES OF TITLE, ACCURACY, AND QUIET ENJOYMENT.</p>
                <ol start="9">
                    <li><strong>Indemnification</strong></li>
                </ol>
                <p>You agree to indemnify and hold Atlas Professional Services L.L.C., its parents, subsidiaries, affiliates, officers, employees, agents, partners and licensors (collectively, the <strong>"Atlas Professional Services L.L.C. Parties"</strong>) harmless from any losses, costs, liabilities and expenses (including reasonable attorneys’ fees) relating to or arising out of: (a) your Data or (b) your violation of any applicable laws, rules or regulations. Atlas Professional Services L.L.C. reserves the right, at its own cost, to assume the exclusive defense and control of any matter requiring indemnification by you, in which event you will fully cooperate with Atlas Professional Services L.L.C. in asserting any available defenses. You agree that the provisions in this section will survive your access or use of the Website.</p>
                <ol start="10">
                    <li><strong>Limitation of Liability</strong></li>
                </ol>
                <p>YOU UNDERSTAND AND AGREE THAT EXCEPT WHERE PROHIBITED IN NO EVENT WILL THE ATLAS PROFESSIONAL SERVICES L.L.C. PARTIES BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE CONTENT OR THE SEXUAL HARASSMENT TRANINIG, CONTENT, INCLUDING, WITHOUT LIMITATION, ANY DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROFITS, WHETHER OR NOT A ATLAS PROFESSIONAL SERVICES L.L.C. PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, ON ANY THEORY OF LIABILITY. THE ATLAS PROFESSIONAL SERVICES L.L.C. PARTIES’ TOTAL CUMULATIVE LIABILITY IN CONNECTION WITH THESE TERMS OF USE, THE SEXUAL HARASSMENT TRANINIG, OR THE CONTENT, WHETHER IN CONTRACT, TORT, OR OTHERWISE, WILL NOT EXCEED FIFTY DOLLARS (US$50). THE SEXUAL HARASSMENT TRANINIG IS NOT INTENDED TO PROVIDE LEGAL ADIVICE AND SHALL NOT BE USED AS A SUBSTITUTE FOR SPEAKING OR COUNSELING WITH AN ATTORNEY, AND USER HEREBY WAIVES ANY ARGUMENT RAISING SAME.</p>
                <ol start="11">
                    <li><strong>International Visitors</strong></li>
                </ol>
                <p>The Website can be accessed from countries around the world and may contain references to products and services that are not available in your country. These references do not imply that Atlas Professional Services L.L.C. intends to provide any product or service offerings in your country. We control and operate the Website, the Content and our offerings from our facilities in the United States of America. Atlas Professional Services L.L.C. makes no representations that the Website, the Content and any of our offerings are or will be appropriate or available for use in foreign countries. Those who access or use Website or the Content from other jurisdictions do so at their own volition and are responsible for compliance with all applicable laws.</p>
                <ol start="12">
                    <li><strong>Amendment(s)</strong></li>
                </ol>
                <p>We may change these Terms of Use from time to time for any reason. If we make any changes, we will change the Last Updated date (found above) and post the new Terms of Use.</p>
                <ol start="13">
                    <li><strong>Additional terms</strong></li>
                </ol>
                <p>We currently have various service offerings. If you subscribe to any such offerings, we will provide such services under a separate digitally or manually executed agreement. Such agreement will supersede these Terms of Use. These Terms of Use may also be superseded by expressly designated legal notices or terms located on particular pages of the Website.</p>
                <ol start="14">
                    <li><strong>Governing Law; Venue</strong></li>
                </ol>
                <p>These Terms of Use and any related action will be governed and interpreted by and under the laws of the State of Arizona, without regard conflicts of laws, principles or rules. Venue for any dispute arising out of these Terms of Use will be the state courts located in Maricopa County, Arizona or the federal courts of the United States in the District of Arizona, and each party (you and Atlas Professional Services L.L.C.) consents to personal jurisdiction to such court(s) and also waive any right it may otherwise have to challenge the appropriateness of such forums.</p>
                <ol start="15">
                    <li><strong>General Provisions</strong></li>
                </ol>
                <p>Any waiver or failure to enforce any provision of these Terms of Use on one occasion will not be deemed a waiver of any other provision or of such provision on any other occasion. If any particular provision of these Terms of Use is held invalid or unenforceable, that part will be modified to reflect the original intention of the parties, and the other parts will remain in full force and effect.</p>
                <ol start="16">
                    <li><strong>Contact Information</strong></li>
                </ol>
                <p>If you have any questions about these Terms of Use, please contact us at www.atlasproserv.com</p>
            </div>
        </div>
    </div>
</section>