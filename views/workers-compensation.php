<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/workers-parallax.jpeg">
    <div class="container">
        <div class="page-title">
            <h1 class="text-white" style="text-shadow: 0px 0px 30px rgba(0, 0, 0, 1);">Workers' Compensation</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
    </div>
</section>


<section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>PEO Workers' Compensation Services</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-5 d-flex align-items-center">
                    <div>
                        <p class="text-justify">EmployeeMaxHR's comprehensive 
                            workers' compensation outsourcing program 
                            includes cost-effective insurance options, 
                            claims administration, and loss control. 
                            On-site inspections, safety program development, 
                            implementation, and OSHA training are available 
                            services, as well.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">We offer solutions 
                            to reduce your exposure by increasing 
                            compliance with federal and state laws. 
                            The outsourcing services provide the peace 
                            of mind needed to let business owners like 
                            you refocus on managing the business!</p>
                        <div class="inside-spacer"></div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/peo.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Protect Your Employees from Workplace Injuries and Accidents</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-6 d-flex align-items-center">
                    <div>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">More than ever, 
                        employers need real solutions to help 
                        them reduce the cost of insurance 
                        coverage and improve business cash 
                        flow. Workers’ compensation provides 
                        both employers and employees protection 
                        for work-related injuries and accidents.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">Our team of 
                            certified payroll professionals 
                            is equipped with over 120 years 
                            of collective experience in 
                            multi-state payrolls, human resources, 
                            customer service, and accounting, 
                            creating the foundation for EmployeeMax’s 
                            leadership in secure, cloud-based HR 
                            and payroll services.</p>
                        <div class="inside-spacer"></div>
                    </div>
                </div>
                <div class="col-lg-6 line-left d-flex align-items-center">
                    <div>
                        <div class="item">
                            <div class="icon text-center"><img src="<?php echo basePathUrl();?>images/employeemax/eliminate.png" alt=""></div>
                            <div class="text">
                                <h5 class="title">Eliminates big premium deposits</h5>
                                <p>Lorem ipsum dolor aset amet lorem 
                                ipsum dolor aset amet.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon text-center"><img src="<?php echo basePathUrl();?>images/employeemax/auditoria.png" alt=""></div>
                            <div class="text">
                                <h5 class="title">Reduces your audit risk</h5>
                                <p>Lorem ipsum dolor aset amet lorem 
                                ipsum dolor aset amet.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon text-center"><img src="<?php echo basePathUrl();?>images/employeemax/flujo.png" alt=""></div>
                            <div class="text">
                                <h5 class="title">Improves business cash flow</h5>
                                <p>Lorem ipsum dolor aset amet lorem 
                                ipsum dolor aset amet.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Suite of Full-Service Workers' Compensation Solutions</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/suite.jpeg" alt=""></a>
                                <div class="inside-spacer"></div>                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex align-items-center">
                    <div>
                        <p class="text-justify">Pay-as-you-go workers' 
                            compensation insurance enables businesses 
                            to buy workers' compensation with little 
                            or no money down. Our exclusive Pay-As-You-Go 
                            programs also help protect employers from 
                            large audit bills because the premium is 
                            based on real-time payroll wages and reporting. 
                            We are affiliated with most of the top insurance 
                            carriers in the United States, which means we 
                            can write a policy for most risks and industries.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">From developing safety 
                            manuals and providing OSHA training to 
                            conducting onsite inspections and presenting 
                            opportunities for open market plans, EmployeeMax's 
                            workers’ compensation team is readily equipped to 
                            mitigate risk to safeguard worksite employees' 
                            safety and compliance. Our risk and safety experts 
                            are available to assist with any questions or 
                            concerns you may have regarding workers’ compensation, 
                            compliance, and other risk and safety-related issues.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section class="orange">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4 class="text-white">Have questions about EmployeeMax?</h4>
                <span class="lead sub-header text-white">See EmployeeMax in Action. Call 888-376-7291</span>
                <br><br>
                <a class="btn contact-section" href="<?php echo basePathUrl();?>PENDING">Schedule a FREE Demo <i
                        class="fas fa-calendar-alt" style="margin-left: 7px;"></i></a>
            </div>
        </div>
    </div>
</section>